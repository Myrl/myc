module RSA where

import System.Random
import Control.Monad.Random
import Control.Monad.Loops

import Primes.Testing
import Powers

generateUntilR :: (MonadRandom m, Random a) => (a -> Bool) -> (a, a) -> m a
generateUntilR p = iterateUntil p . getRandomR

generatepq :: MonadRandom m => Int -> m (Integer, Integer)
generatepq n = do
    let half = n `div` 2
        min = half - (half `div` 16)
    p <- until isPrime succ <$> getRandomR (2^min, 2^half - 1)
    let qmin = ((2^n) `div` p) + 1
        qmax = ((2^(n + 1)) `div` p)
    q <- generateUntilR isPrime (qmin, qmax)
    return (p, q)
